const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
async function getAcronym() {
  while (output.definitions.length < 10) {
    var acronym = randomstring.generate(3).toUpperCase();
    try{
      let response = await got(inputURL+acronym);
      console.log("got data for acronym", acronym);
      console.log("add returned data to definitions array");
      output.definitions.push(response.body);
    } catch(err){
      console.log(err)
    }
    
  }

}
console.log("calling looping function");
getAcronym().then(() => {
  console.log("saving output file formatted with 2 space indenting");
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  });
});
