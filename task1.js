const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output={};

jsonfile.readFile(inputFile, function(err, body){
    console.log("loaded input file content", body);
    var names=body.names;
    output.emails=[];

    for(let i=0;i<names.length;i++){
        var str=names[i].split("").reverse().join("");
        output.emails.push(str+randomstring.generate(5)+"@gmail.com")
    }
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
  });